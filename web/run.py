# Bootstrap lib
import os
import sys
import signal
import json
import glob

# Bottle bootstrapping
import bottle
from bottle import route, view, request, abort, post, static_file, run

# Session Middleware
import logging
from beaker.middleware import SessionMiddleware

# Adir Modules
from adir.projectoverview import projectoverview
from adir.metadataeditor import metadataeditor
from adir.containerrendering import containerrendering
from adir.publicationoverview import publicationoverview
import adir.crypto
import adir.chiliarchiving
import adir.transfertemplate
import adir.chiliauth
from adir.util import load_config, conf

# Global Variables
WEBROOT = os.path.abspath(os.path.dirname(__file__))
bottle.TEMPLATE_PATH = [os.path.join(WEBROOT, 'views')]
STATIC_DIR = os.path.join(WEBROOT, 'static')
PID = str(os.getpid())
PIDFILE = "/tmp/adir-archive.pid"

def make_app():    
    sessionmgr = adir.chiliauth.chiliauth()
    transfertemplatemgr = adir.transfertemplate.transfertemplate()
    crypto = adir.crypto.crypto()
    chiliarchiving = adir.chiliarchiving.chiliarchiving()
    
    ################### FRONPAGE REDIRECTION ##############
    @route('/')
    def redirect_portal():
        bottle.redirect('http://astrofit-archive.gwdg.de/runtime/node/1', 303)
    
    ###################  LOGIN ################ 
    
    @route('/login')
    @view('login_form')
    def login_form():
        path = conf('adir.datadir')
        projectfolders = [f for f in os.listdir(path) if os.path.isdir(os.path.join(path, f))]
        projects = '<script>(function(){var projects = new Array(); '
        for item in projectfolders:
            projects += 'projects.push("' + item + '"); '
        projects += 'loginManager.setAvailableProjects(projects);})();</script>'
        return dict(projects=projects)
    
    @route('/login', method='POST')
    def login():
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        project = request.POST.get('project', '')
        if sessionmgr.login(username, password, project):
            return '{"ok":"true"}'
        else:
            abort(401);
        
    @route('/myprojects', method='POST')
    def joinedprojects():
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        bottle.response.content_type = 'application/json'
        return json.dumps(sessionmgr.getJoinedProjects(username, password))
        
    @route('/logout')
    def logout():
        sessionmgr.logout('/login')
        
    ###################  OVERVIEW PAGE ################
    
    # Overviewpoage
    @route('/overview')
    @view('index')
    def projectOverviewGET():
        sessionmgr.protectResource('/login')
        currentproject = sessionmgr.getUserProject()
        overview = projectoverview(currentproject, sessionmgr);
        return dict(relative_path='../',
                    overview_link='./overview',
                    postbox_link='./postbox',
                    publications_link='./publications',
                    archive_link='http://astrofit-archive.gwdg.de/runtime/node/4',
                    project=currentproject,
                    userdetails=sessionmgr.getUserDetails(),
                    content=overview.getRenderedOverview())
        
    ################## PUBLICATION PAGE #################       
    
    @route('/publications')
    @view('index')
    def publicationOverviewGET():
        sessionmgr.protectResource('/login')
        currentproject = sessionmgr.getUserProject()
        publicationrenderer = publicationoverview();
        return dict(relative_path='../',
                    overview_link='./overview',
                    postbox_link='./postbox',
                    publications_link='#',
                    archive_link='http://astrofit-archive.gwdg.de/runtime/node/4',
                    project=currentproject,
                    userdetails=sessionmgr.getUserDetails(),
                    content=publicationrenderer.getRenderedOverview())
    
    ###################  POSTBOX ################
    
    @route('/postbox')
    @view('index')
    def postboxOverviewGET():
        sessionmgr.protectResource('/login')
        currentproject = sessionmgr.getUserProject()
        overview = projectoverview(currentproject, sessionmgr);
        return dict(relative_path='../../',
                    overview_link='../overview',
                    publications_link='./publications',
                    postbox_link='#',
                    archive_link='http://astrofit-archive.gwdg.de/runtime/node/4',
                    project=currentproject,
                    userdetails=sessionmgr.getUserDetails(),
                    content=overview.getRenderedPostBoxOverview(),
                    showpostbox=True)
    
    ################### SHOW TRANSFER CONTAINER ########
    
    @route('/show.exe', method='POST')
    @view('index')
    def showTransferContainer():
        sessionmgr.protectResource('/login')
        currentproject = sessionmgr.getUserProject()
        containerrend = containerrendering(currentproject)
        secid = request.POST.get('secid', 'INVALID_DEFAULT_VALUE')
        typeselection = request.POST.get('type', 'INVALID_DEFAULT_VALUE')
        entity = request.POST.get('entity', 'INVALID_DEFAULT_VALUE')
        if(crypto.isValidSecureIdForActions(entity, secid, 'showentity')):
            content=containerrend.getRenderedContent(typeselection, entity, secid)
        else:
            content='Invalid SecureID supplied.'
        return dict(relative_path='../../',
                    overview_link='../../overview',
                    postbox_link='../../postbox',
                    publications_link='./publications',
                    archive_link='http://astrofit-archive.gwdg.de/runtime/node/4',
                    project=currentproject,
                    userdetails=sessionmgr.getUserDetails(),
                    content=content)
        
    ################### ARCHIVE CONTAINER ########
    
    @route('/archive.exe', method='POST')
    @view('index')
    def archiveContainer():
        sessionmgr.protectResource('/login')
        currentproject = sessionmgr.getUserProject()
        secid = request.POST.get('secid', 'INVALID_DEFAULT_VALUE')
        entity = request.POST.get('entity', 'INVALID_DEFAULT_VALUE')
        firstname = sessionmgr.getUserFirstname()
        lastname = sessionmgr.getUserLastname()
        email = sessionmgr.getUserMail()
        if(crypto.isValidSecureIdForActions(entity, secid, 'archivecontainer')):
            content='<div id="archiveresult"></div>'
            showarchiving=True
            result=chiliarchiving.startArchiving(entity, firstname, lastname, email, currentproject)
        else:
            content='Invalid SecureID supplied.'
            showarchiving=False
            result=''
        return dict(relative_path='../../',
                    overview_link='../../overview',
                    postbox_link='../../postbox',
                    publications_link='../../publications',
                    archive_link='http://astrofit-archive.gwdg.de/runtime/node/4',
                    project=currentproject,
                    userdetails=sessionmgr.getUserDetails(),
                    content=content,
                    result=result,
                    showarchiving=showarchiving)
    
    ################### METADATA EDITOR ################
    
    @route('/metadataeditor.exe', method='POST')
    @view('index')
    def metadataEditor():
        sessionmgr.protectResource('/login')
        currentproject = sessionmgr.getUserProject()
        secid = request.POST.get('secid', 'INVALID_DEFAULT_VALUE')
        entity = request.POST.get('entity', 'INVALID_DEFAULT_VALUE')
        if (crypto.isValidSecureIdForActions(entity, secid, 'metadataeditor')):
            mde = metadataeditor(currentproject, 'postbox', entity);
            return dict(relative_path='../../../',
                        overview_link='../../overview',
                        postbox_link='/../../postbox',
                        publications_link='../../publications',
                        archive_link='http://astrofit-archive.gwdg.de/runtime/node/4',
                        userdetails=sessionmgr.getUserDetails(),
                        project=sessionmgr.getUserProject(),
                        content='<div id="metadataeditor">metadataeditor</div>',
                        formcontent=mde.getFormHtmlAsJsonString(),
                        formjavascriptcode=mde.getTemplateJavaScript(),
                        metadata=mde.getMetaDataAsJson(),
                        showmetadataeditor=True)
        else:
            abort(401, 'Sorry, you supplied a wrong signature.')
        
    ################## METADATA Storage ###############
    @post('/storemetadata.exe')
    def storeMetaData():
        sessionmgr.protectResource('/login')
        json_content = request.forms.keys()[0]
        parsed_content = json.loads(json_content)
    
        # Extract Content
        project = parsed_content['ticket']['project']
        entity = parsed_content['ticket']['entity']
        secid = parsed_content['ticket']['secid']
        data = parsed_content['data']
        formname = parsed_content['formname']
        
        # Decode input
        secid = secid.decode("utf8").replace("'", "")
        project = project.decode("utf8").replace("'", "")
        entity = entity.decode("utf8").replace("'", "")
        
        if(crypto.isValidSecureIdForActions(project + entity + formname, secid, 'storemetadata')):
            currentproject = sessionmgr.getUserProject()
            mde = metadataeditor(currentproject, 'postbox', entity);
            mde.storeMetadataOnDisk(data, formname)
            return {'ok':'true'}
        else:
            return {'error':'secureid_invalid'}
    
    ################### SSH KEY-Serving ################
    
    @route('/sshkeys/:filename')
    def sshKeyDownload(filename):
        sessionmgr.protectResource('/login')
        projectdetails = sessionmgr.getUserProjectDetails()
        filename = projectdetails['sshkeyfile']
        return static_file(filename, root=conf('adir.userkeys'))
    
    ################### Transfer-Folder-Template-Serving ################
    
    @route('/newtransfertemplate')
    def transferTemplateDownload():
        sessionmgr.protectResource('/login')
        userid = sessionmgr.getUserName()
        firstname = sessionmgr.getUserFirstname()
        lastname = sessionmgr.getUserLastname()
        email = sessionmgr.getUserMail()
        projectname = sessionmgr.getUserProject()
        zipfilename = transfertemplatemgr.generateNewTemplate(userid, firstname, lastname, email, projectname)
        
        zipdir = os.path.dirname(zipfilename)
        zipname = os.path.basename(zipfilename)
        return static_file(zipname, root=zipdir, download=zipname)
    
    ###################  STATIC CONTENT ################
    
    @route('/favicon.ico')
    def getFavicon():
        return static_file('favicon.ico', STATIC_DIR)
    
    @route('/static/:path#.+#', name='static')
    def static(path):
        return static_file(path, root=STATIC_DIR)
    
    ############### Bash-Script Notification String #####
    @route('/updater/uploadscript.json')
    def getCurrentVersion():
        currentversion ={}
        currentversion['build'] = '110'
        return currentversion
    
    ###################  BOTTLE RUNTIME ################
    
    
    app = bottle.app()
    logging.basicConfig(level=logging.DEBUG)
    session_opts = {
        'session.validate_key': True,
        'session.cookie_expires': True,
        'session.timeout': 3600 * 24,  # 1 day
        'session.encrypt_key': 'dsabjkfhi389hf2q980nKKKK29k2kwk000s',
        'session.type': 'file',
        'session.data_dir': '/tmp/adir-archive_sessions/',
        'session.auto': True
        }
    
    
    app = SessionMiddleware(app, session_opts)
    return app

def init_config():
    for filename in sorted(glob.glob('/etc/adir-archive/*.cfg')):
        if os.path.exists(filename):
            load_config(filename) 
    
    if 'ADIR_CONFIG' in os.environ:
        for path in os.environ['ADIR_CONFIG'].split(':'):
            load_config(path)

def signal_handler(signal, frame):
    logging.info('ADIR-Archive is shutdown. Removing PID-File.')
    os.unlink(PIDFILE)
    sys.exit(0)
        
def start_application_production():
    signal.signal(signal.SIGINT,signal_handler)
    signal.signal(signal.SIGTERM,signal_handler)
    if os.path.isfile(PIDFILE):
        print "A PID-File was found. %s already exists, ADIR Archive exits." % PIDFILE
        sys.exit(-1)
    else:
        file(PIDFILE, 'w').write(PID)
        os.chmod(PIDFILE, 0644)
        logging.info('== Starting ADIR ARCHIVE in PRODUCTION mode ==')
        run(make_app(), server='cherrypy', host='127.0.0.1', port=8080, reloader=False, debug=False)

        
def start_application_debug():
    logging.info('== Starting ADIR ARCHIVE in DEBUG mode ==')
    run(make_app(), host='127.0.0.1', port=8080, reloader=True, debug=True)

if __name__ == '__main__':
    init_config()
    if 'ADIRDBG' in os.environ:
        start_application_debug()
    else:
        start_application_production()
        
else:
    init_config()
    app = make_app()

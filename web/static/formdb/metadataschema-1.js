// Section for transfer-types
metadataset.ticket = {};
metadataset.ticket.secid = '{{secid}}';
metadataset.ticket.project = '{{project}}';
metadataset.ticket.entity = '{{entity}}';

// Section for rules
metadataset.rules = null;

// Section for templates
metadataset.option = null;

// Matadata Form Specific Functions
metaDataFormManager = function() {
	var obj = {};

	obj.removeTemplateInstance = function(domElem) {
		domElem.children().last().remove();
		domElem.children().last().remove();
		var tmpformdata = metadataEditorObject.getData();
		metadataEditorObject.setForm(metadataset.html, metadataset.options,
				metadataset.rules, tmpformdata);
	};
	
	obj.addTemplateInstance = function(domElem, options, data){
		FormTools.addTemplateInstance(domElem, options, data);
	};

	return obj;
}();

<div id="sshdialog"
	title="Download SSH transfer key for Project {{project}}"
	style="width: 200px;">
	<p>Download SSH-Key pair to transfer your data into the postbox of
		project {{project}}</p>
	<p>Note the key pair is only valid for project {{project}}</p>
	<p>
		<a href="{{relative_path}}sshkeys/ssh-transfer-key-{{project}}.zip">Download
			Key</a>
</div>
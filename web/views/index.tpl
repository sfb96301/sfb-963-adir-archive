<!DOCTYPE html>
<head>

<title>CRC 963 - Research Data Archive - Logged in as
	{{project}}</title>
<!-- Le styles -->
<link href="{{relative_path}}static/css/bootstrap.min.css"
	rel="stylesheet">
<link href="{{relative_path}}static/css/jquery-ui-1.10.3.custom.min.css"
	rel="stylesheet">
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}
</style>
<link href="{{relative_path}}static/css/bootstrap-responsive.css"
	rel="stylesheet">
	<script src="{{relative_path}}static/js/json3.js"></script>
	<script src="{{relative_path}}static/js/jquery.js"></script>
	<script src="{{relative_path}}static/js/jquery-ui.js"></script>
	<script src="{{relative_path}}static/js/all.js"></script>
</head>

<body>

	 <div class="id-navbar navbar navbar-medium navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<button type="button" class="btn btn-navbar" data-toggle="collapse"
					data-target=".nav-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<span class="brand" style="font-weight: bold;"><img src="static/img/gwdglogo.png">ADIR CRC 963 Research Archive -
					Project {{project}}</span>
				<div class="nav-collapse collapse">
					<ul class="nav">
						<li class="active"><a href="http://astrofit-archive.gwdg.de/runtime/node/1">Home</a></li>
						<li class="active"><a href="http://astrofit-archive.gwdg.de/runtime/node/3">Documentation</a></li>
						<li class="active"><a href="http://astrofit-archive.gwdg.de/runtime/node/2">Tutorial</a></li>
						<li class="active"><a href="#">Manage Data</a></li>
						<li class="active"><a href="http://astrofit-archive.gwdg.de/runtime/node/4">Archived Datasets</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<!-- /container -->
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span3">
				<div class="well sidebar-nav">
					<ul class="nav nav-list">
						<li class="nav-header">Sidebar</li>
						<li><span id="userinfo"></span></li>
						<li><a href="{{overview_link}}">Overview</a></li>
						<li><a href="{{postbox_link}}">Postbox</a></li>
						<li><a href="{{archive_link}}">Archive</a></li>
						<li><a href="{{publications_link}}">Publications</a></li>
						<li><a href="#"
							onclick="$('#sshdialog').dialog('open'); return false;">Transfer
								Key</a></li>
						<li><a href="{{relative_path}}newtransfertemplate">New
								Transfer Container</a></li>
						<li><a href="#" onclick="$('#aboutdialog').dialog('open'); return false;">About</a></li>
						<li><a href="{{relative_path}}logout">Logout</a></li>
					</ul>
				</div>
				<!--/.well -->
			</div>
			<!--/span-->
			<div class="span9">
				<div class="hero-unit">
					{{!content}}
					%if get('showmetadataeditor'):
					<button type="button" class="btn btn-primary" id="savemetadata" onclick="metadataEditorObject.saveMetaData(); return false;">Save
						Metadata</button>
					%end
					%if get('showpostbox'):
					<br />
					<p>
						Transfer containers are listed, if they contain the file <i>receipt.json</i>.<br />
						Only owners of transfer containers can edit metadata and archive files.
					</p>
					<br />
					%end
				</div>
				%include sshdialog project=project,relative_path=relative_path
				%include aboutdialog project=project
				%include archivedialog
			</div>
		</div>
	</div>
	<!-- /container -->

	<!-- Le javascript
    ================================================== -->
	<script>
	     var userdetails={{!userdetails}};
	       $(document).ready(function() {
	           var html = userdetails.firstname + ' ' + userdetails.lastname + '<br />';
	           html += '(' + userdetails.email + ')';
	           $('#userinfo').html(html);
	         });
	</script>
	%if get('showmetadataeditor'):
	<script>
	
        var metadataset = {};
        metadataset.html = {{!formcontent}};
        // Add more structures from JS-Formdefinition
        {{!formjavascriptcode}}
        
        // Transform JSON-Structs
        var metadatatmp = {{!metadata}};
        metadataset.data = metadatatmp.data;
        metadataset.formname = metadatatmp.formname;
        	
        // Call the Editor
        metadataEditorObject.setForm(metadataset.html, metadataset.options, metadataset.rules, metadataset.data);
 </script>
	%end
	%if get('showarchiving'):
  <script>
        $(document).ready(function() {
        	var result = {{!result}};
        	var htmlcode = '<p>';
        	if(result.result == 'success'){
        		htmlcode += 'Congratulation! Your archiving request is now processed.<br />This will take several days. ';
        		htmlcode += '<br/ >When finished, your transfer container will appear in the archive.';
        	}
        	else
        		htmlcode += 'Archiving failed. Please contact the system administrator. We are sorry for this. :-(';
        	htmlcode += '</p><p><button class="btn btn-primary" type="button" onclick="window.location.href=\'./postbox\';">';
        	htmlcode += 'continue with postbox</button></p>';
        	$('#archiveresult').html(htmlcode);
        });
 </script>
  %end
</body>
</html>

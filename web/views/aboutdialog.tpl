<div id="aboutdialog" title="About ADIR Archive - {{project}}">
	<p>
		<b>ADIR ARCHIVE</b>
	</p>
	<p>
	<p>For help, suggestions or bugs write an E-Mail to</p>
	<p>
		<span id="mail1"></span>
	</p>
	<hr />
	<p>
		<b>Build Information:</b>
	</p>
	<p>Build 170 - Kallisto</p>
	<p>
		<b>Previous releases:</b>
	</p>
	<p>Build 150 - Mariner</p>
	<p>Build 120 - Galaxy Plus</p>
	<p>Build 110 - Galaxy - Initial Release</p>
</div>
<div id="archivedialog" title="Request Archiving" style="width: 500px;">
	<p>Do you want to archive the container to tape?</p>
	<p>Please note that archiving may require human interaction at the
		data center like adding new tapes.</p>
	<p>Hence, this function is not meant for testing.</p>
	<hr />
	<p>In the following an archive operation is requested. It may take
		several days, until the archiving operation is successful. After that,
		the container will appear in the archive</p>
</div>
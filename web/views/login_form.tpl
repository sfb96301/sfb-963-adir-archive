<!DOCTYPE html>
<html>
<head>

<title>CRC 963 - Research Data Archive</title>
<!-- Le styles -->
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}
</style>
<link href="static/css/bootstrap-responsive.css" rel="stylesheet">

</head>

<body>

	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<button type="button" class="btn btn-navbar" data-toggle="collapse"
					data-target=".nav-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<span class="brand" style="font-weight: bold;"><img src="static/img/gwdglogo.png"> ADIR CRC 963 Research Archive</span>
				<div class="nav-collapse collapse">
					<ul class="nav">
						<li class="active"><a href="http://astrofit-archive.gwdg.de/runtime/node/1">Home</a></li>
						<li class="active"><a href="http://astrofit-archive.gwdg.de/runtime/node/3">Documentation</a></li>
						<li class="active"><a href="http://astrofit-archive.gwdg.de/runtime/node/2">Tutorial</a></li>
						<li class="active"><a href="#">Manage Data</a></li>
						<li class="active"><a href="http://astrofit-archive.gwdg.de/runtime/node/4">Archived Datasets</a></li>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
		</div>
	</div>

	<!-- Warning Modal -->
	<div id="default_warning" class="alert fade in" style="display: none;">
	</div>
	 <!-- /container -->
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span3">
				<div class="well sidebar-nav">
					<ul class="nav nav-list">
						<li class="nav-header">&nbsp;</li>
					</ul>
				</div>
				<!--/.well -->
			</div>
			<!--/span-->
			<div class="span9">
				<div class="hero-unit">

					<div id="loginscreen">
						<h3>
							<span class="login_heading">Welcome to <strong>ADIR
									Archive</strong>!
							</span>
						</h3>

						<div>Please login with your GWDG account:</div>

						<div class="input-group">
							<input type="text" class="form-control"
								placeholder="GWDG USER ID" id="username">
						</div>
						<div class="input-group">
							<input type="password" id="password" class="form-control"
								placeholder="XXXXXXX">
						</div>
						<div class="input-group">
							<button type="button" class="btn btn-primary"
								onclick="loginManager.getAndRenderAvailableProjects();">Login</button>
						</div>
						<div>&nbsp;</div>
						<div style="text-align: left;">
							For archiving data in your CRC project, your Chili Project
							account needs to be activated.<br />Please write an E-Mail <span
								id="mail1"></span> or to <span id="mail2"></span> for requesting
							activation.
						</div>
					</div>

					<div id="loadingscreen" style="display: none; text-align: left;">
						Working ... <img src="static/img/loading.gif" />
					</div>

					<div id="projectscreen" style="display: none; text-align: left;">
						<h3>Your project memberships</h3>
						<div id="projecttable"></div>
						<div>
							<br />
							<br />
							<button type="button" class="btn btn-info"
								onclick="location.reload();">logout</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		<!-- /container -->

		<!-- Search Result Modal -->
		<div id="default_dialog" class="modal hide fade" tabindex="-1"
			role="dialog" aria-labelledby="searchResultsModalLabel"
			aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">X</button>
				<h3 id="modal_label">HEADER</h3>
			</div>
			<div class="modal-body" id="modal_body">TEXT</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal"
					aria-hidden="true">Close</button>
			</div>
		</div>

		<!-- Le javascript
    ================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="static/js/jquery.js"></script>
		<script src="static/js/jquery-ui.js"></script>
		<script src="static/js/all.js"></script>
		<!-- Global loader -->
		<script>
			$(document).ready(
					function() {
						$('#mail1').html(
								emailManager.encodeEmail(emailManager
										.getOliver()));
						$('#mail2').html(
								emailManager.encodeEmail(emailManager
										.getHarald()));
					});
		</script>
		{{!projects}}
</body>
</html>
all: build

install: build
	pip install -r requirements.txt
	pip install -U .

build: web/static/js/all.js
	./setup.py sdist

web/static/js/all.js: js_development/*.js
	java -jar scripts/compiler.jar --js js_development/* --js_output_file web/static/js/all.js 2> /dev/null
	java -jar scripts/compiler.jar --js js_development/fmt.js  --js js_development/landingpage.js --js_output_file landingpage_template/js/all.js 2> /dev/null



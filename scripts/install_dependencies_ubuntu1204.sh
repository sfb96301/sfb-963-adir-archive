#!/bin/bash
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi
echo Installing Python virtual environment
apt-get install python-virtualenv python-dev python2.7-dev build-essential openjdk-6-jre-headless -y
apt-get install uwsgi-plugin-python -y
echo
echo "All required software installed sucessfully"

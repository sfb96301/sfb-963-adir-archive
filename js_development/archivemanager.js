$(document).ready(function() {
	$("#archivedialog").dialog({
		autoOpen : false,
		width: 500,
		buttons : {
			"start archiving" : function() {
				archiveManager.processSelection(true);
			},
			"cancel" : function() {
				archiveManager.processSelection(false);
			}
		}
	});
});
var archiveManager = function() {
	var obj = {};
	obj.entity = '';
	obj.secid = '';
	obj.formDivNotCreatedBefore = true;

	obj.showArchiveDialog = function(entity, secid) {
		$('#archivedialog').dialog('open');
		obj.entity = entity;
		obj.secid = secid;
	};
	
	obj.processSelection = function(isArchivingWished){
		$('#archivedialog').dialog("close");
		if(isArchivingWished)
			obj.runArchiving(obj.entity, obj.secid);
	};
	
	obj.runArchiving = function(entity, secid){
		// dynamically generate the form
		if (obj.formDivNotCreatedBefore){
			var $div = $('<div />').appendTo('body');
			$div.attr('id', 'archiveformdiv');
			$div.attr('style', 'display:none;');
			obj.formDivNotCreatedBefore = false;
		}
		var htmlcode = '<form id="archiveform" action="archive.exe" method="post">';
		htmlcode += '<input type="hidde" name="entity" value="' + entity + '" />';
		htmlcode += '<input type="hidde" name="secid" value="' + secid + '" />';
		htmlcode += '</form>';
		$('#archiveformdiv').html(htmlcode);
		$('#archiveform').submit();
	};

	return obj;
}();

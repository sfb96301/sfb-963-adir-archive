var publications = function() {
	var obj = {};

	obj.renderPublications = function(htmlcontent) {
		var rawhtml = htmlcontent.content;
		rawhtml = $("<textarea/>").html(rawhtml).val();
		$('#publications').html(rawhtml);
	};
	return obj;
}();
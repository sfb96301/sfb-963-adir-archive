// Email obfuscator script 2.1 by Tim Williams, University of Arizona
// Random encryption key feature by Andrew Moulden, Site Engineering Ltd
// This code is freeware provided these four comment lines remain intact
// A wizard to generate this code is at http://www.jottings.com/obfuscator/
// Extended by Oliver Schmitt - GWDG 2013
var emailManager = function() {
	var obj = {};

	obj.encodeEmail = function(data) {
		var key = data.key;
		var coded = data.coded;
		var shift = coded.length;
		var link = '';
		var ltr = '';
		for (var i = 0; i < coded.length; i++) {
			if (key.indexOf(coded.charAt(i)) == -1) {
				ltr = coded.charAt(i);
				link += (ltr);
			} else {
				ltr = (key.indexOf(coded.charAt(i)) - shift + key.length)
						% key.length;
				link += (key.charAt(ltr));
			}
		}
		return link;
	};

	obj.getOliver = function() {
		var data = {};
		data.coded = "6UJwXp.3iVzJEE@HsOH.OX";
		data.key = "bujmipxdoR98nUCLIQZtFsa5WzJAKO67ghqeTkfvME43crNSDY20BlHV1XyGPw";
		return data;
	};

	obj.getHarald = function() {
		var data = {};
		data.coded = "GszTyh@THZze.LGPHUM.yhU-VeaZZUhVah.fa";
		data.key = "LQeP2SAYt5GRbkTzFNqKiVOlHpvoyfEDWJChBImar0X3wZgx6sMj8ud7cU49n1";
		return data;
	};

	return obj;
}();
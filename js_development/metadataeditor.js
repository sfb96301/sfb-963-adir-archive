var metadataEditorObject = function() {
	var obj = {};

	var my = {
		rules : null
	};

	obj.getData = function() {
		try {
			return FormTools.readElements('#metadataeditor', my.rules);
		} catch (e) {
			alert(e);
		}
	};

	obj.setForm = function(html, options, rules, data) {
		my.rules = rules;
		$('#metadataeditor').html(html);
		FormTools.fillElements('#metadataeditor', options, data);
		FormTools.readElements('#metadataeditor', my.rules);
	};

	obj.saveMetaData = function() {
		// Collect Data
		var dataToSend = {};
		dataToSend.ticket = {};
		dataToSend.data = metadataEditorObject.getData();
		dataToSend.formname = metadataset.formname;
		dataToSend.ticket = metadataset.ticket;

		// Debug-Output
		console.log(JSON.stringify(dataToSend.data));
		// return

		// Post the data to the server
		$.ajax({
			type : 'POST',
			url : '../storemetadata.exe',
			data : JSON.stringify(dataToSend),
			contentType : 'application/json; charset=utf-8',
			dataType : 'json',
			success : function(succ) {
				alert('Metadata saved sucessfully!');
				metadataEditorObject.setForm(metadataset.html,
						metadataset.options, metadataset.rules,
						dataToSend.data);
				url = "../postbox";
				$(location).attr('href',url);
			},
			error : function(err) {
				alert('ERROR Saving metadata failed!');
			}
		});
	};

	return obj;
}();
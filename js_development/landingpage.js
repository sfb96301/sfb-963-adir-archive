var landpageObject = function() {
	var obj = {};

	var my = {
		rules : null
	};

	obj.setForm = function(html, options, rules, data) {
		my.rules = rules;
		$('#metadataeditor').html(html);
		$('.btn').hide();
		FormTools.fillElements('#metadataeditor', options, data);
		FormTools.readElements('#metadataeditor', my.rules);
	};

	obj.renderMetadata = function(jsonFileName) {
		// Read Metadata from file
		var json = (function() {
			var json = null;
			$.ajax({
				'async' : false,
				'global' : false,
				'url' : "./" + jsonFileName,
				'dataType' : "json",
				'success' : function(data) {
					json = data;
				}
			});
			return json;
		})();

		var templateFileName = json.formname + ".html";

		// Read html from file
		var formHtml = (function() {
			var formHtml = null;
			$.ajax({
				'async' : false,
				'global' : false,
				'url' : "./formdb/" + templateFileName,
				'dataType' : "html",
				'success' : function(data) {
					formHtml = data;
					// Do some inline replacements
					var handle = JSON.stringify(json.data.handle).replace('"',
							'').replace('"', '');
					var handle_url = "http://hdl.handle.net/" + handle;
					var handle_link = '<td>Longterm URL: <a href="'
							+ handle_url + '">' + handle_url + '</a></td>';
					var archivepath = JSON.stringify(json.data.archivepath).replace('"',
					'').replace('"', '');
					formHtml = formHtml.replace("{{uploader_fullname}}", '<a href="./' + jsonFileName + '">Download Metadaten</a>');
					formHtml = formHtml.replace("{{uploader_project}}", "");
					formHtml = formHtml.replace("<h2>Uploader</h2>",
							"<h2>Landing page</h2>");
					formHtml = formHtml.replace("<td>Fullname</td>",
							handle_link);
					formHtml = formHtml.replace("{{uploader_email}}", "Archive-Path: " + archivepath);
					formHtml = formHtml.replace("<td>E-Mail</td>", "");
					formHtml = formHtml.replace("<td>Project</td>", "");
					formHtml = formHtml.split("(mandatory)").join(''); //str.split(search).join(replacement)
				}
			});
			return formHtml;
		})();

		// Render the entire form
		obj.setForm(formHtml, null, null, json.data);

	};

	return obj;
}();
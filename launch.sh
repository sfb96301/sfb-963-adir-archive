#!/bin/bash
# Bootstrap Python environment

make

test -d venv || virtualenv venv
venv/bin/pip install -r requirements.txt
venv/bin/pip install -U .

venv/bin/python -m adir.web.run


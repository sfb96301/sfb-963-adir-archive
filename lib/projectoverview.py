import os
import json
from crypto import crypto
from util import datapath

class projectoverview(object):
    def __init__(self, projectname, sessionmgr):
        self.projectname = projectname
        self.sessionmgr = sessionmgr
        self.crypto = crypto()
        
    def getRenderedOverview(self): 
        returnval = '<h2>Welcome to ' + self.projectname + '</h2>'
        unfinished_elements = len(self.__getPostboxContent());
        # We have to remove one element, as the metadata-folder should not be counted
        unfinished_elements = unfinished_elements - 1
        if(unfinished_elements > 0):
            returnval += 'You have ' + str(unfinished_elements) + ' elements in you postbox.'
        else:
            returnval += 'Your postbox is empty.'
        return returnval

    def getRenderedArchiveOverview(self):
        entity_list= self.__getArchiveContent()
        returnval = ''
        returnval += '<h2>Archive Content</h2><table class="table">'
        for entity in entity_list:
            returnval += '<td><img src="static/img/safe.png" alt=""/></td>'
            returnval += '<td>' + entity + '</td>'
            # Show Button
            returnval += '<td><form action="show.exe" method="post" id="showform">'
            returnval += '<button class="btn btn-default" type="button" onclick="$(\'#showform\').submit();">show</button>'
            returnval += '<input type="hidden" name="secid" value="' + self.crypto.createSecureIdForActions(entity, 'showentity') + '" />'
            returnval += '<input type="hidden" name="entity" value="' + entity + '" />'
            returnval += '<input type="hidden" name="type" value="archive" />'
            returnval += '</form></td>'
        returnval += '</table>'
        return returnval
    
            
    def __getArchiveContent(self):
        
        archive_path = datapath(self.projectname, 'archive')
        archiv_content = [f for f in os.listdir(archive_path) if os.path.isdir(os.path.join(archive_path, f))]
        return archiv_content
    
    def getRenderedPostBoxOverview(self):
        entity_list = self.__getPostboxContent()
        returnval = ''
        returnval += '<h2>Postbox Content</h2>'
        returnval += '<table class="table">'
        for entity in entity_list:
            if(not(self.__isReceiptIncluded(entity))): # Skip containers without receipt.json
                continue
            state = self.__getArchiveState(entity)
            returnval += '<tr>'
            returnval += '<td><img src="static/img/cart.png" alt=""/></td>'
            returnval += '<td>' + entity + '</td>'
            # Show Button
            #returnval += '<td><form action="show.exe" method="post" id="showform">'
            #returnval += '<button class="btn btn-default" type="button" onclick="$(\'#showform\').submit();">show</button>'
            #returnval += '<input type="hidden" name="secid" value="' + self.crypto.createSecureIdForActions(entity, 'showentity') + '" />'
            #returnval += '<input type="hidden" name="entity" value="' + entity + '" />'
            #returnval += '<input type="hidden" name="type" value="postbox" />'
            #returnval += '</form></td>'
            #returnval += '<td>'
            # Archive Button
            if(state == 'idle'): 
                if(self.__isContainerOwned(entity)): # Skip containers without receipt.json
                    # Metadatabutton
                    securityid = self.crypto.createSecureIdForActions(entity, 'metadataeditor')
                    returnval += '<td><form action="metadataeditor.exe" method="post" id="entermetadataform_' + securityid + '">'
                    returnval += '<button class="btn btn-default" type="button" onclick="$(\'#entermetadataform_' + securityid +'\').submit();">enter metadata</button>'
                    returnval += '<input type="hidden" name="secid" value="' + securityid + '" />'
                    returnval += '<input type="hidden" name="entity" value="' + entity + '" />'
                    returnval += '</form></td>'
                    if (self.__isMetadataAvailable(entity)):
                        returnval += '<td><button class="btn btn-primary" type="button" onclick="archiveManager.showArchiveDialog(\'' + entity + '\',\''+ self.crypto.createSecureIdForActions(entity, 'archivecontainer') + '\');">start archiving</button></td>'
                    else:
                        returnval += '<td>enter metadata to archive</td>'
                else:
                    returnval += '<td>owned by other user</td>'
            elif(state == 'archived'):
                returnval += '<td>Archived - <a href="' + self.__getArchivePersistentURL(entity) + '" target="_blank">view data set</a></td>'
            elif(state == 'waiting'):
                returnval += '<td>archiving requested</td>'
            returnval += '</tr>'
        returnval += '</table>'
        return returnval
    
            
    def __getPostboxContent(self):
        postbox_path = datapath(self.projectname, 'postbox')
        postboxcontent = [f for f in os.listdir(postbox_path) if os.path.isdir(os.path.join(postbox_path, f))]
        return postboxcontent
    
    def __isMetadataAvailable(self, entity):
        try:
            metadatapath = datapath(self.projectname, 'postbox', 'metadata' , str(entity + '_metadata.json'))
            with open(metadatapath) as fp:
                fp.read()
            return True
        except:
            return False
    
    def __getArchiveState(self, entity):
        try:
            path = datapath(self.projectname, 'postbox', 'metadata', str(entity + '_archive.json'))
            with open(path) as fp:
                return json.load(fp)['state']
        except:
            return 'idle'
    
    def __getArchivePersistentURL(self, entity):
        try:
            path = datapath(self.projectname, 'postbox', 'metadata', str(entity + '_archive.json'))
            with open(path) as fp:
                return json.load(fp)['persistent_url']
        except:
            return ''
    
    def __isReceiptIncluded(self, entity):
        try:
            self.__readReceiptContent(entity)
            return True
        except:
            return False
    
    def __isContainerOwned(self,entity):
        receipt_dict = self.__readReceiptContent(entity)
        if (not(self.crypto.isValidReceipt(receipt_dict))):
            return False
        if (receipt_dict['data']['firstname'] == self.sessionmgr.getUserFirstname()):
            if (receipt_dict['data']['lastname'] == self.sessionmgr.getUserLastname()):
                return True
        return False
    
    def __readReceiptContent(self, entity):
        receiptpath = datapath(self.projectname, 'postbox', entity, 'receipt.json')
        with open(receiptpath) as fp:
            return json.load(fp)
        
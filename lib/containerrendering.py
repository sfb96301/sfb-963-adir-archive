import os
import json
from util import datapath

class containerrendering(object):
    def __init__(self, projectname):
        self.projectname = projectname
        
    def getRenderedContent(self, type, entity, secid):
        # Render Receipt
        output = '<h2>Receipt</h2>'
        output += self.renderReceiptAsHtml(type, entity)
        
        # Render Metadata
        output += '<h2>Metadata</h2>'
        output += self.renderMetadataAsHtml(type, entity)
        
        #Render Filecontent
        output += '<h2>Uploaded files and folders</h2>'
        output += self.list_files(type, entity)
        return output
    
    def renderReceiptAsHtml(self, type, entity):
        json_content = self.getFileContent(type, entity, 'receipt.json')
        json_content = json_content.decode("utf8")
        parsed_content = json.loads(json_content)
        data=parsed_content['data']
        signature=parsed_content['signature']
        output = ''
        #Display Data
        for key in data:
            output += key + ': ' + data[key] + '\n'
        # Display Signature
        # TODO: Add Verification of Signature
        output += '\nsignature: ' + signature
        return '<pre>' + output + '</pre>'
    
    def renderMetadataAsHtml(self, type, entity):
        try:
            json_content = self.getFileContent(type, entity, 'metadata.json')
            json_content = json_content.decode("utf8")
            data = json.loads(json_content)
        except:
            return 'No Metadata entered yet.'
        output = ''
        #Display Data
        for key in data:
            output += key + ': ' + data[key] + '\n'
        # Display Signature
        # TODO: Add Verification of Signature
        return '<pre>' + output + '</pre>'
    
    def getFileContent(self, type, entity, filename):
        with open(datapath(self.projectname, type, entity, filename)) as fp:
            return fp.read()

    def list_files(self, type, entity):
        path = datapath(self.projectname, type, entity)
        output = ''
        for root, dirs, files in os.walk(path):
            level = root.replace(path, '').count(os.sep)
            indent = ' ' * 4 * (level)
            output += indent + os.path.basename(root) + '/\n' 
            subindent = ' ' * 4 * (level + 1)
            for f in files:
                output += subindent + f + '\n'
        return '<pre>' + output + '</pre>'

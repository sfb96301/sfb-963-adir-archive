import bottle
from bottle import request, abort
import os
import json
import httplib2
import hashlib
import string
import random
from util import conf

class chiliauth(object):
    
    def __init__(self):
        self.__users = []
        # Read the user database
        with open(conf('adir.userdb')) as fp:
            data = json.load(fp)
            self.__users.extend(data['users'])
            
    def getJoinedProjects(self,username, password):
        data = {}
        data['projects'] = []
        
        if self.__isChiliCredentialsOk(username, password) or self.__isDebugLoginOk(username, password): 
            for current_user in self.__users:
                current_id = str(current_user['id']).lower()
                current_projects = current_user['projects']
                # If username is found, investigate groups
                if current_id == username:
                    for current_project in current_projects:
                        data['projects'].append(current_project['projectid'])
                    return data
        else:
            abort(401);
    
    def __isChiliCredentialsOk(self, username, password):
        h = httplib2.Http('/tmp/adir-archiv_http-cache', disable_ssl_certificate_validation=True)
        h.add_credentials(username, password)
        resp, content = h.request("https://projects.gwdg.de/users/current.json")
        if resp['status'] == '200':
            return True
        else:
            return False
        
    def __isDebugLoginOk(self, username, password):
        credentialfile = conf('adir.debug-handle')
        if os.path.isfile(credentialfile):
            with open(credentialfile) as fp:
                debugfiledata = ''.join(fp).strip().split(':')
                pwdhash, salt = debugfiledata
                calculatedPwdHash = hashlib.sha256(password + salt).hexdigest()
                if (username == 'hdebug' and pwdhash == calculatedPwdHash):
                    return True
        return False
    
    def login(self, username, password, project):
        # Iterate through all user objects
        for current_user in self.__users:
            current_id = str(current_user['id']).lower()
            current_projects = current_user['projects']
            # If username is found, investigate groups
            if current_id == username:
                for current_project in current_projects:
                    # project found, try chili login
                    if current_project['projectid'] == project:
                        # Try login with chili
                        if self.__chililogin(username, password, project, current_user, current_project):
                            return True
        # Nothing was found that is fail -> do the redirection
        return False
    
    def isLoggedIn(self):
        try:
            beaker_session = bottle.request.environ['beaker.session']
            if beaker_session['login']:
                return True
        except:
            return False
        return False
    
    def protectResource(self, failurl):
        if self.isLoggedIn() == False:
            bottle.redirect(failurl, 303)
            
    def getUserDetails(self):
        retvalue = {}
        if request.environ['beaker.session']['login'] == True:
            retvalue['firstname'] = request.environ['beaker.session']['firstname'] 
            retvalue['lastname'] = request.environ['beaker.session']['lastname']
            retvalue['email'] = request.environ['beaker.session']['mail']
            retvalue['project'] = request.environ['beaker.session']['project']
        else:
            raise('User not logged in')
        return json.dumps(retvalue)
        
    def getUserFirstname(self):
        if request.environ['beaker.session']['login'] == False:
            raise('User not logged in')
        else:
            return request.environ['beaker.session']['firstname']
        
    def getCurrentUser(self):
        if request.environ['beaker.session']['login'] == False:
            raise('User not logged in')
        else:
            return request.environ['beaker.session']['userobject']
        
    def getUserProjectDetails(self):
        if request.environ['beaker.session']['login'] == False:
            raise('User not logged in')
        else:
            return request.environ['beaker.session']['projectobject']
        
    def getUserLastname(self):
        if request.environ['beaker.session']['login'] == False:
            raise('User not logged in')
        else:
            return request.environ['beaker.session']['lastname']
        
    def getUserMail(self):
        if request.environ['beaker.session']['login'] == False:
            raise('User not logged in')
        else:
            return request.environ['beaker.session']['mail']
        
    def getUserProject(self):
        if request.environ['beaker.session']['login'] == False:
            raise('User not logged in')
        else:
            return request.environ['beaker.session']['project']
        
    def getUserName(self):
        if request.environ['beaker.session']['login'] == False:
            raise('User not logged in')
        else:
            return request.environ['beaker.session']['username']
        
    def getUserSessionNonce(self):
        if request.environ['beaker.session']['login'] == False:
            raise('User not logged in')
        else:
            return request.environ['beaker.session']['sessionnonce']
    
        
    def logout(self, url):
        request.environ['beaker.session']['login'] = False
        request.environ['beaker.session']['username'] = ''
        request.environ['beaker.session']['password'] = ''
        request.environ['beaker.session']['project'] = ''
        request.environ['beaker.session']['lastname'] = ''
        request.environ['beaker.session']['firstname'] = ''
        request.environ['beaker.session']['mail'] = ''
        request.environ['beaker.session']['userobject'] = ''
        request.environ['beaker.session']['projectobject'] = ''
        request.environ['beaker.session']['sessionnonce'] = ''
        bottle.redirect(url, 303)
        
    def __chililogin(self, username, password, project, userobject, projectobject):
        if self.__debugLogin(username, password, project, userobject, projectobject):
            return True
        
        h = httplib2.Http('/tmp/adir-archiv_http-cache', disable_ssl_certificate_validation=True)
        h.add_credentials(username, password)
        resp, content = h.request("https://projects.gwdg.de/users/current.json")
        if resp['status'] == '200':
            data = json.loads(content)
            # pprint('data:')
            # pprint(data)
            request.environ['beaker.session']['login'] = True
            request.environ['beaker.session']['username'] = username
            request.environ['beaker.session']['project'] = project
            request.environ['beaker.session']['lastname'] = data['user']['lastname']
            request.environ['beaker.session']['firstname'] = data['user']['firstname']
            request.environ['beaker.session']['mail'] = data['user']['mail']
            request.environ['beaker.session']['userobject'] = userobject
            request.environ['beaker.session']['projectobject'] = projectobject
            request.environ['beaker.session']['sessionnonce'] = self.__generateNonce()
            return True
        return False
    
    def __debugLogin(self, username, password, project, userobject, projectobject):
        if self.__isDebugLoginOk(username, password):
            request.environ['beaker.session']['login'] = True
            request.environ['beaker.session']['username'] = username
            request.environ['beaker.session']['project'] = project
            request.environ['beaker.session']['lastname'] = 'Debugger'
            request.environ['beaker.session']['firstname'] = 'Harald'
            request.environ['beaker.session']['mail'] = 'harald.debugger@example.net'
            request.environ['beaker.session']['userobject'] = userobject
            request.environ['beaker.session']['projectobject'] = projectobject
            request.environ['beaker.session']['sessionnonce'] = self.__generateNonce()
            return True
        return False
    
    def __generateNonce(self):
        size = 32
        chars = string.ascii_uppercase + string.digits + string.ascii_lowercase
        return ''.join(random.choice(chars) for _ in range(size))
        


import json
from containerrendering import containerrendering
from crypto import crypto
from util import datapath
import os
#from pprint import pprint


class metadataeditor(object):
    def __init__(self, projectname, typeselection, entity):
        self.projectname = projectname
        self.typeselection = typeselection
        self.entity = entity
        self.crypto = crypto()
        self.containerrendering = containerrendering(projectname)
        self.metadatafilepath = datapath(self.projectname, 'postbox', 'metadata' , str(self.entity + '_metadata.json'))
        self.metadata = self.__getMetaDataAsDict()
        self.formdbdir = os.path.dirname(__file__) + '/web/static/formdb/'
        
    def getTemplateJavaScript(self):
        # Write Securing code for upload
        content = self.__readFileFromDiskAsUTF8String(self.formdbdir + self.__getLatestMetadataFormRevisionname() + '.js')
        content = content.replace('{{project}}', self.projectname)
        content = content.replace('{{entity}}', self.entity)
        content = content.replace('{{secid}}', self.crypto.createSecureIdForActions(str(self.projectname) + str(self.entity) + str(self.metadata['formname']), 'storemetadata'))
        return content
        
        
    def getFormHtmlAsJsonString(self):
        # Read the template
        content = self.__readFileFromDiskAsUTF8String(self.formdbdir + self.__getLatestMetadataFormRevisionname() + '.html')
        
        # Read the receipt
        json_content = self.containerrendering.getFileContent(self.typeselection, self.entity, 'receipt.json')
        parsed_content = json.loads(json_content)
        data = parsed_content['data']
        
        # Display the receipt
        uploader_fullname = data['firstname'] + ' ' + data['lastname']
        uploader_email = data['email']
        uploader_project = data['projectname']
    
        content = content.replace('{{uploader_fullname}}', uploader_fullname)
        content = content.replace('{{uploader_email}}', uploader_email)
        content = content.replace('{{uploader_project}}', uploader_project)
        
        return json.dumps(content)
    
    def getMetaDataAsJson(self):
        return json.dumps(self.metadata)
    
    def __getMetaDataAsDict(self):
        retdict = {}
        try:
            #pprint(self.metadatafilepath)
            filecontent = self.__readFileFromDiskAsUTF8String(self.metadatafilepath).replace('\n', '')
            filecontent = json.loads(filecontent)
            retdict['formname'] = filecontent['formname']
            retdict['data'] = filecontent['data']
        except:
            #If no meta data file is present, then return an empty JSON structure
            retdict['formname'] = self.__getLatestMetadataFormRevisionname();
            retdict['data'] = {}
        return retdict
    
    # TODO FIXME MAKE ME  DYNAMIC
    def __getLatestMetadataFormRevisionname(self):
        return 'metadataschema-1'
    
    def __readFileFromDiskAsUTF8String(self, filename):
        with open(filename) as fp:
            return fp.read().decode('utf8')
    
    def storeMetadataOnDisk(self, metadata, formname):
        dictforstoring = {}
        dictforstoring['data'] = metadata
        dictforstoring['formname'] = formname

        with open(self.metadatafilepath, 'w') as fp:
            json.dump(dictforstoring, fp, sort_keys=True, indent=4)

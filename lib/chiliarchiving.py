import json
import httplib2
from crypto import crypto
from util import conf, datapath

class chiliarchiving(object):
    
        def __init__(self):
            self.crypto = crypto()
            # Set the project-ID of the related chili project
            self.projectid = 366
            # Read the API-Key
            with open(conf('chili.keyfile')) as fp:
                self.apikey = fp.read().strip()

        def startArchiving(self, entity, firstname, lastname, email, projectname):
            receipt_dict=self.__readReceiptContent(entity, projectname)
            transferId=receipt_dict['data']['transferid']
            if(not(self.crypto.isValidReceipt(receipt_dict))):
                return self.__createJsonResultMessage('invalid_receipt')
            if(not(self.__isContainerOwned(entity, firstname, lastname, projectname))):
                return self.__createJsonResultMessage('container_not_owned')
            try:
                json_content = self.__sendArchiveNotification(transferId, firstname, lastname, email, projectname)
                json_content = json_content.decode("utf8")
                data = json.loads(json_content)
                chiliprojectticketid = data['issue']['id']
                created_on = data['issue']['created_on']
                self.__setArchiveFlag(entity, projectname, chiliprojectticketid, created_on)
                return self.__createJsonResultMessage('success')
            except:
                return self.__createJsonResultMessage('error')

        def __readReceiptContent(self, entity, projectname):
            receiptpath = datapath(projectname, 'postbox', entity, 'receipt.json')
            with open(receiptpath) as fp:
                return json.load(fp)
        
        def __isContainerOwned(self, entity, firstname, lastname, projectname):
            receipt_dict = self.__readReceiptContent(entity, projectname)
            if (receipt_dict['data']['firstname'] == firstname):
                if (receipt_dict['data']['lastname'] == lastname):
                    return True
                return False

        def __createJsonResultMessage(self, resultmessage):
            returnmessage = {}
            returnmessage['result'] = resultmessage 
            return json.dumps(returnmessage)
            
        def __sendArchiveNotification(self, transferId, firstname, lastname, email, projectname):
            messagetext = self.__generateNotificationMessage(transferId, firstname, lastname, email, projectname)
            http = httplib2.Http()
            http.add_credentials(self.apikey, 'XXXXXXXXXXXXX')  # No Password needed, as we use the API-Key
            http.disable_ssl_certificate_validation = True
            resp, content = http.request(uri='https://projects.gwdg.de/issues.json', method='POST', headers={'Content-Type': 'application/json'}, body=messagetext)
            if resp['status'] == '201':
                return content;
            else:
                errormessage = 'Unable to contact chili project server'
                print(content)
                print(errormessage)
                raise Exception(errormessage)
            return content

        def __generateNotificationMessage(self, transferId, firstname, lastname, email, projectname):
            # {
            # "issue": {
            #    "project_id": 366,
            #    "subject": "testsubject",
            #    "description": "testdescription"
            # }
            # }
            data = {}
            data['issue'] = {}
            data['issue']['project_id'] = self.projectid
            data['issue']['subject'] = 'Archiving of Transfer-Container: ' + transferId + ' requested'
            data['issue']['description'] = 'Project: ' + projectname + ' Contactdetails: '
            data['issue']['description'] += firstname + ' ' + lastname + ' ' + email + ' TransferID: ' + transferId
            serialedTojson = json.dumps(data)
            return serialedTojson
        
        # States for archiving
        # idle     - ready for archiving - no file archive.json existing (start state)
        # waiting  - waiting for archive activity (intermediate state)
        # archived - file in the archive (final state)
        #
        def __setArchiveFlag(self, entity, projectname, chiliprojectticketid, created_on):
            data = {
                'chiliprojectticketid': chiliprojectticketid,
                'created_on': created_on,
                'state': 'waiting',
                'persistent_url': ''
            }
            path = datapath(projectname, 'postbox', 'metadata', str(entity + '_archive.json'))
            with open(path, 'w+') as fp:
                json.dump(data, fp)

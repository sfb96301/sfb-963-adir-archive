import os
import zipfile
import json
import time
import datetime
import uuid
from crypto import crypto
from util import conf, workdir
import shutil

class transfertemplate(object):
    
    def __init__(self):
        self.crypto = crypto()
        self.workdir = workdir('transfer')
        
    def generateNewTemplate(self, userid, firstname, lastname, email, projectname):
        transferId = str(uuid.uuid4())
        foldername = 'transfer_container_' + transferId[0:13]
        filename = foldername + '.zip' 
        self.__copyTemplateContent(foldername)
        self.__generateNewReceipt(foldername, transferId, firstname, lastname, email, projectname)
        self.__generateVariables(foldername, projectname, userid)
        self.__zipdir(self.workdir + '/' + filename, self.workdir + '/tmp/' + foldername)
        return self.workdir + '/' + filename

    def __generateNewReceipt(self, foldername, transferId, firstname, lastname, email, projectname):
        
        # Generated signed values
        data = {}
        data['data'] = {}
        
        timestamp =  str(datetime.datetime.utcfromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S +0000'))
        data['data']['receiptgeneration'] = timestamp
        data['data']['transferid'] = transferId
        data['data']['firstname'] = firstname
        data['data']['lastname'] = lastname
        data['data']['email'] = email
        data['data']['projectname'] = projectname
        data['formatversion'] = '1'
        data['signature'] = self.crypto.signReceipt(data)
        
        path = str(self.workdir + '/tmp/' + foldername + '/receipt.json')
        with open(path, 'w') as fp:
            json.dump(data, fp, sort_keys=True, indent=4)
    
    def __generateVariables(self, foldername, projectid, userid):
        with open(self.workdir + '/tmp/' + foldername + '/variables.sh', 'w') as fp:
            fp.write('export projectid=' + projectid + '\n')
            fp.write('export userid=' + userid + '\n')

    def __zipdir(self, filename, path):
        zipf = zipfile.ZipFile(filename, 'w')

        for root, dirs, files in os.walk(path):
            for currentfile in files:
                if dirs == '123':
                    continue
                # Trim the path for the zip file
                relativepath = str(root).split('/')
                relativepath = '/'.join(str(x) for x in relativepath[4:len(relativepath)]) + '/'
                zipf.write(os.path.join(root, currentfile), relativepath + currentfile)
        zipf.close()

    def __copyTemplateContent(self, foldername):
        src = conf('adir.transfer-template')
        dst = os.path.join(self.workdir, 'tmp')
        dst = os.path.join(dst, foldername)
        try:
            shutil.rmtree(dst)
        except:
            pass
        shutil.copytree(src, dst)

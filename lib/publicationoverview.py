import httplib2
import json
import cgi
from util import conf

class publicationoverview(object):
        
    def getRenderedOverview(self):
        publicationaddress = conf('adir.publicationaddress')
        h = httplib2.Http('/tmp/adir-archiv_http-cache', disable_ssl_certificate_validation=True)
        resp, content = h.request(publicationaddress)
        if resp['status'] != '200':
            jsonwrapper = '{"content":"Could retrieve data from the server."}'
        else:
            try:
                content = self.__extractDiv(content)
                content = cgi.escape(content)
                jsonwrapper = {}
                jsonwrapper['content'] = content.decode('utf-8', 'ignore')
                jsonwrapper = json.dumps(jsonwrapper)
            except:
                jsonwrapper = '{"content":"Could encode data from the server."}' 
        output = '<div id="publications">Loading publications from the server of the university <img src="./static/img/loading.gif" /></div>'
        output += "<script>$(document).ready(function() {publications.renderPublications(" + jsonwrapper + ");});</script>"
        return output;
    
    def __extractDiv(self, content): 
        startfound = False
        skipline = 3;
        output = ''
        for currentline in content.splitlines():
            if "rumpf" in currentline:
                startfound = True
            if startfound:
                if skipline > 0:
                    skipline = skipline - 1
                    continue
                if '</div>' in currentline:
                    break
                output += currentline
            else:
                continue;
        return output;

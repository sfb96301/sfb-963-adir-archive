import tempfile
import os
import ConfigParser

__config = dict()
__paths  = dict()

MISSING=object()
def conf(key, default=MISSING):
    if default is MISSING:
        return __config[key];
    else:
        return __config.get(key, default)

def load_config(filename):
    cp = ConfigParser.ConfigParser()
    cp.read(filename)
    for sec in cp.sections():
        for opt in cp.options(sec):
            __config[sec.lower() + "." + opt.lower()] = cp.get(sec, opt).strip()

def workdir(name):
    if name not in __paths:
        __paths[name] = tempfile.mkdtemp(prefix='adir-archive_', suffix='_'+name);
    return __paths[name]

def workfile(dirname, name):
    return os.path.join(workdir(dirname), name)

def datapath(*parts):
    return os.path.join(conf('adir.datadir'), *parts)

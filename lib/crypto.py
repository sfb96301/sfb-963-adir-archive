import hashlib
import hmac
from chiliauth import chiliauth
from util import conf

class crypto(object):
    def __init__(self):
        self.sessionmgr = chiliauth()
        # Check of file with default credentials for debug mode is existing
        with open(conf('adir.keyfile')) as fp:
            self.signingkey = fp.read().strip()
                
    def createSecureIdForActions(self, data, contractname):
        return hmac.new(self.signingkey + self.sessionmgr.getUserSessionNonce() + contractname, data, hashlib.sha256).hexdigest()
        
    def isValidSecureIdForActions(self, data, secid, contractname):
        return (secid == self.createSecureIdForActions(data, contractname))
    
    def signReceipt(self, receipt_dict):
        allSignatures = ''
        # Concatination of hmacs uses values sorted by keyname
        for key, value in sorted(receipt_dict['data'].iteritems()):
            allSignatures += key + ':' + value
        return hmac.new(self.signingkey, allSignatures, hashlib.sha256).hexdigest()
    
    def isValidReceipt(self, receipt_dict):
        try:
            signature = receipt_dict['signature']
            if (signature == self.signReceipt(receipt_dict)):
                return True
            else:
                return False
        except:
            False
        
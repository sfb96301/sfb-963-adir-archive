#!/usr/bin/env python2

import tempfile
import json
import subprocess
import random
import string
import zipfile
import shutil
import os
import sys
import time


def sh(*args):
    subprocess.check_call(args)


def wait():
    raw_input('  Press ENTER (Ctrl+C to abort)')


if len(sys.argv) > 3:
    user = sys.argv[1]
    email = sys.argv[2]
    projects = sys.argv[3:]
else:
    user = raw_input('Username: ').strip()
    email = raw_input('Email: ').strip()
    projects = raw_input('Projects (space separated): ').strip().split()

passwd = ''.join(random.choice(string.letters + string.digits) for _ in range(10))

print "Setup user %r..." % user
userdir = '/home/' + user

if os.path.exists(userdir):
    print "  User exists. Continue?"
    wait()
else:
    print "  Create new system user?"
    wait()
    subprocess.check_call(['adduser', '--disabled-password',
                           '--ingroup', 'upsw', user])

sshdir = userdir + '/.ssh'
if not os.path.exists(sshdir):
    os.mkdir(sshdir, 0700)
    sh('chown', '%s:upsw' % user, sshdir)

authkeyfile = sshdir + '/authorized_keys'
if not os.path.exists(authkeyfile):
    open(authkeyfile, 'w').close()
    sh('chown', '%s:upsw' % user, authkeyfile)

print "Entering temp directory..."

tmp = tempfile.mkdtemp('adir-user')
os.chdir(tmp)

for project in projects:
    print "Setting up project %r ..." % project

    pdir = '/storages/isilon/projects/%s' % project
    if os.path.exists(pdir):
        print "  Project exists :)"
    else:
        print "  Create new project %r?" % project
        wait()
        os.mkdir(pdir, 0755)
        os.mkdir(pdir + '/archive', 0755)
        os.mkdir(pdir + '/postbox', 0775)  # umask changes this to 0755
        os.chmod(pdir + '/postbox', 0775)  # Make sure the group can write
        os.mkdir(pdir + '/postbox/metadata', 0755)
        sh('chown', '-R', 'sfb96301:upsw', pdir)

    print "  Loading user database..."
    dbfile = '/etc/adir-archive/userdb/users.json'
    with open(dbfile) as fp:
        db = json.load(fp)

    for userentry in db['users']:
        if userentry['id'] == user:
            break
    else:
        userentry = dict(id=user, projects=[])
        db['users'].append(userentry)

    for i, projectentry in enumerate(userentry['projects']):
        if projectentry['projectid'] == project:
            print "  User already has access to this project."
            print "  Continue and create new key?"
            wait()
            del userentry['projects'][i]

    print "  Creating ssh key..."
    keyname = project + '_rsa'
    sh('ssh-keygen', '-trsa', '-C', email, '-f', keyname, '-N', passwd)

    with open(keyname + '.pub', 'r') as kfp:
        pubkey = kfp.read().strip()

    print "  Packaging key archives..."
    keyzip = '/etc/adir-archive/userdb/ssh-keys/'
    keyzip += '%s_upload_key_%s_%s.zip'
    keyzip %= (time.strftime('%Y%m%d'), project, user)
    zip = zipfile.ZipFile(keyzip, 'w')
    zip.write(keyname, 'id_rsa')
    zip.write(keyname + '.pub', 'id_rsa.pub')
    zip.close()
    sh('chown', 'sfb96301:sfb96301', keyzip)

    with open(keyzip + '.pass', 'w') as fp:
        fp.write(passwd + '\n')
    os.chmod(keyzip + '.pass', 0600)

    print "  Updating userdb..."
    shutil.copy(dbfile, dbfile + '.bak')

    userentry['projects'].append(
        dict(projectid=project, sshkeyfile=os.path.basename(keyzip))
    )

    with open(dbfile, 'w') as fp:
        json.dump(db, fp, indent=2, separators=(',', ': '))

    print "  Installing key..."

    with open(sshdir + '/authorized_keys', 'a') as fp:
        fp.write('command="')
        fp.write('rsync --server -vvvgtre.iLs --specials --delete-after')
        fp.write(' . /home/%s/%s/postbox/' % (user, project))
        fp.write('" ')
        fp.write(pubkey)
        fp.write("\n")

    print "  Creating symlink..."
    os.symlink('/storages/isilon/projects/' + project, userdir + '/' + project)

    print "  DONE"


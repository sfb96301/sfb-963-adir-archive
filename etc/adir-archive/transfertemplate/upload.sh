#!/bin/bash

########### ENVIRONMENT VARIABLES ################
buildversion=140
identity=`printf '%q\n' "${PWD##*/}"`
TPWD=`pwd`

########## FUNCTION DEFINTIONS ###################
function pause(){
	read -p "$*"
}

function nop(){
	echo " " > /dev/null
}

function banner(){
	echo
	echo '== ADIR UPLOAD SCRIPT BUILD: '$buildversion' =='
	echo
	VERSIONHASH=`curl https://cdstar-prod01.gwdg.de/updater/uploadscript.json --insecure -o - 2> /dev/null|md5sum`
	if [[ $VERSIONHASH == 2aae4dd2367e44* ]]; then
		nop
	else
		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		echo "!!                                             !!"
		echo "!!   Warning this upload script is outdated.   !!"
		echo "!!    Please use a new transfer-container.     !!"
		echo "!!                                             !!"
		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		echo
	fi
}

function checkhostname(){
	if [[ $HOSTNAME == adirarchive || $HOSTNAME == gwdu* ]]; then
		nop
	else
		echo "This script works best on gwdu05, gwdu06, gwdu101 or gwdu102."
		echo "But we try it now with your local installation."
		echo ""
		echo "This script requires: curl, scp, ssh and a working internet connection"
	fi
	if [ $HOSTNAME = "gwdu05" ]; then
		echo "Hint: Uploading from this host (gwdu05) is slow. Consider using gwdu06, gwdu101 or gwdu102 for uploading."
	fi
}

########### CHECK IF LOCATION HAS BEEN CALLED PROPERPLY ##########
function checkdir(){
	if [ -f ./receipt.json ]; then
		echo "Transfer Container-Name: "$identity
		echo 
	else
		echo "You cannot call the upload script outside the transfer container. Exiting."
		exit
	fi
}

########## CHECK HOSTNAME ##############
function runhostnamecheck(){
	if [ -n "$ADIRDBG" ]; then
		echo "Host name check turned off."
	else
		checkhostname
	fi
	echo
}

banner
checkdir
runhostnamecheck

############# Checking files ############

echo -ne 'Checking presents of necessary files ...\n\n'

# Check receipt
if [ -f ./receipt.json ]
then
	echo -ne 'receipt.json, '
else
	echo 'Receipt not found. Exiting'
	exit
fi

# Check SSH-Key
if [ -f ./transfer-keys/id_rsa ]
then
  echo -ne 'id_rsa, '
else
	echo 'Upload keys not found. Exiting'
	exit
fi

# Check Exclude
if [ -f ./exclude.txt ]
then
    	echo -ne 'exclude.txt, '
else
	echo 'Exclude file not found. Exiting'
	exit
fi

# Check Variables
if [ -f ./variables.sh ]
then
    	echo -ne 'variables.sh, '
else
	echo 'variables.sh file not found. Exiting'
	exit
fi
echo -ne ' OK\n'

############## CHECK VARIABLES #################
source ./variables.sh
if [ -n "$userid" ]; then
	echo 'User Id setup correctly? ... Yes, user Id is: '$userid
else
	echo 'variables.sh does not contain a valid userid. Exiting.'
fi
if [ -n "$projectid" ]; then
	echo 'Project Id setup correctly? ... Yes, project Id is: '$projectid
else
	echo 'variables.sh does not contain a valid projectid. Exiting.'
fi


# Start the Transfer
echo 
pause 'Press [Enter] to upload [CRTL+C] to cancel...'
echo

# Secure transfer-keys
chmod 0600 transfer-keys/id_rsa 2> /dev/null
chmod 0600 transfer-keys/id_rsa.pub 2> /dev/null
echo
echo 'Starting transfer ...'
echo


rsync \
	--human-readable --progress --stats \
 	--recursive --times --group \
	--specials \
	--delete-after \
	--links \
	--copy-dirlinks \
	--chmod=g+rX \
	--exclude-from './exclude.txt'\
	../$identity -e 'ssh -p 1247 -i ./transfer-keys/id_rsa' $userid@cdstar-prod01.gwdg.de:/home/$userid/$projectid/postbox/


#!/usr/bin/env python

from setuptools import setup

setup(name='adir',
      version='1.0',
      description='adir-archive',
      author='oschmitt',
      author_email='oschmitt@gwdg.de',
      #url='http://www.python.org/sigs/distutils-sig/',
      packages=['adir', 'adir.web'],
      package_dir = {'adir': 'lib',
                     'adir.web': 'web'},
      include_package_data=True,
      zip_safe=False,
 )
